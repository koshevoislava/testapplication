Ext.define('MyApp.store.Campaings', {
    extend: 'Ext.data.Store',

    requires : [
        'MyApp.model.Campaings'
    ],

    storeId : "Campaings",
    alias   : "store.campaings",
    model   : "MyApp.model.Campaings",

    data: { items: [
        {
            "number"        : "1",
            "id"            : "1",
            "name"          : "Campaings1",
            "start-date"    : "12-02-2016 00:00:00",
            "end-date"      : "09-02-2017 00:00:00",
            "status"        : "Scheduled",
            "tasks-count"   : "10",
            "description"   : "Some comments",
            "created-by"    : "Petya",
            "se-oc"         : false,
            "se-ot"         : false,
            "failures"      : false,
            "cc"            : null
        },
        { number : "2", "id" : "2", "name" : "Campaings2", "start-date" : "02-02-2016 00:00:00", "end-date" : "02-02-2016 00:00:00", "status" : "Running", "tasks-count" : "2", "description" : "First Comment", "created-by" : "Vasya", "se-oc" : false, "se-ot" : false, "failures" : false, "cc": null },
        { number : "3", "id" : "3", "name" : "Campaings3", "start-date" : "02-12-2016 00:00:00", "end-date" : "12-02-2016 00:00:00", "status" : "Running", "tasks-count" : "5", "description" : "Second comment", "created-by" : "Vasya", "se-oc" : false, "se-ot" : false, "failures" : false, "cc": null },
        { number : "4", "id" : "4", "name" : "Campaings4", "start-date" : "12-02-2016 00:00:00", "end-date" : "02-02-2016 00:00:00", "status" : "Stoped", "tasks-count" : "7", "description" : "Third comment", "created-by" : "Valera", "se-oc" : false, "se-ot" : false, "failures" : false, "cc": null }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
