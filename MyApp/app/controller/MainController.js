
Ext.define('MyApp.controller.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onDeleteClick : function(){
        var grid = Ext.getCmp('mainlist');
        if(grid){
            var sm = grid.getSelectionModel();
            var rowSelected = sm.getSelection();
            if (!rowSelected.length) {
                Ext.Msg.alert('Warning', 'No Records Selected');
                return;
            }
            Ext.Msg.confirm('Remove Record', 'Are you sure?', function (button) {
                if (button == 'yes') {
                    if(rowSelected.length > 1){
                        for(var i = 0 ; i < rowSelected.length; i++){
                            grid.store.remove(rowSelected[i]);
                        }
                        return;
                    }
                    grid.store.remove(rowSelected[0]);
                }
            });
        }

    },

    onCreateClick : function(){

        var grid = Ext.getCmp('mainlist');
        var store = grid.store, nextRowIndex = store.data.length + 1;
        store.add({number : nextRowIndex, id : nextRowIndex})

    },

    onRefreshClick : function(){
        var grid = Ext.getCmp('mainlist');
        grid.getView().refresh(true)
    },

    onChangeMailCheckbox : function(checkbox, checked){
        Object.getOwnPropertyNames(checked).length ? Ext.getCmp('cc').enable() : Ext.getCmp('cc').disable()
    },

    onRowSelect : function(grid, record){

        //Enable fields
        var formPanelName = Ext.ComponentQuery.query('#detailform field');

        for(var i= 0; i < formPanelName.length; i++) {
            if(formPanelName[i].id == 'cc') continue;
            formPanelName[i].setDisabled(false);
        }

        //Enable buttons
        var formButtons = Ext.ComponentQuery.query('#detailform button');

        for(var i= 0; i < formButtons.length; i++) {
            formButtons[i].setDisabled(false);
        }

        var detailView = Ext.ComponentQuery.query('DetailForm')[0];
        detailView.getViewModel().setData({ rec: record });
    },

    onSavedClick : function(btn){
        var campaingsView = btn.up('DetailForm');
        var record = campaingsView.getViewModel().getData().rec;
        if (record) {
            record.commit();
            Ext.Msg.alert('Success', 'Record saved!');
        }
        else {
            Ext.Msg.alert('Fail', 'Try again.');
        }
    },

    onCancelClick : function(btn){
        var campaingsView = btn.up('DetailForm');
        var record = campaingsView.getViewModel().getData().rec;
        if (record) {
            record.reject();
            Ext.Msg.alert('Success', 'Record rejected');
        }
        else {
            Ext.Msg.alert('Fail', 'Try again.');
        }
    },

    //Filters

    onChangeFilterName : function(field, newValue) {
        Ext.getCmp('mainlist').store.clearFilter();
        Ext.getCmp('mainlist').store.filter([{
            property: 'name',
            anyMatch: true,
            value   : newValue
        } ]);
    },

    onClickFilterSchedule : function(){
        Ext.getCmp('mainlist').store.filter([{
            property: 'status',
            value: 'Scheduled',
            anyMatch: true
        } ]);
    },

    onClickFilterRun : function(){
        Ext.getCmp('mainlist').store.filter([{
            property: 'status',
            value: 'Running',
            anyMatch: true
        } ]);
    },

    onClickFilterStop : function(){
        Ext.getCmp('mainlist').store.filter([{
            property: 'status',
            value: 'Stoped',
            anyMatch: true
        } ]);
    },

    onClickCleanFilters : function(){
        Ext.getCmp('mainlist').store.clearFilter();
    }
});
