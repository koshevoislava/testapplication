Ext.define('MyApp.model.Campaings', {
    extend: 'Ext.data.Model',


    fields: [
        {
            name : 'number',
            type : 'int'
        },
        {
            name : 'id',
            type : 'int'
        },
        {
            name : 'name',
            type : 'string'
        },
        {
            name : 'start-date',
            type: 'date',
            dateFormat: 'm-d-Y H:i:s'
        },
        {
            name : 'end-date',
            type: 'date',
            dateFormat: 'm-d-Y H:i:s'
        },
        {
            name : 'status',
            type : 'string'
        },
        {
            name : 'tasks-count',
            type : 'int'
        },
        {
            name : 'description',
            type : 'string'
        },
        {
            name : 'created-by',
            type : 'string'
        },
        {
            name : 'se-oc',
            type : 'boolean'
        },
        {
            name : 'se-ot',
            type : 'boolean'
        },
        {
            name : 'failures',
            type : 'boolean'
        },
        {
            name : 'cc',
            type : 'string'
        }
    ]
});
