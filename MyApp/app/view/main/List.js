/**
 * This view is an example list of people.
 */

Ext.define('MyApp.view.main.List', {
    extend:  'Ext.grid.Panel',
    xtype: 'MainGrid',
    id: 'mainlist',

    store: {
        type: 'campaings'
    },



    tbar: [
        {
            text: 'Create',
            iconCls: 'x-fa fa-plus',
            listeners: {
                click: 'onCreateClick'
            }
        },
        {
            text: 'Delete',
            iconCls: 'x-fa fa-trash-o',
            listeners: {
                click: 'onDeleteClick'
            }
        },
        {
            xtype: 'tbseparator'
        },
        {
            text: 'Refresh',
            iconCls: 'x-fa fa-refresh',
            listeners: {
                click: 'onRefreshClick'
            }
        },
        {
            xtype: 'tbseparator'
        },
        {
            text: 'Schedule',
            iconCls: 'x-fa fa-check-circle-o',
            listeners: {
                click: 'onClickFilterSchedule'
            }
        },
        {
            text: 'Run',
            iconCls: 'x-fa fa-play',
            listeners: {
                click: 'onClickFilterRun'
            }
        },
        {
            text: 'Stop',
            iconCls: 'x-fa fa-stop',

            menu: [{
                text: 'Filter stoped statuses',
                iconCls: 'x-fa fa-stop',
                listeners: {
                    click: 'onClickFilterStop'
                }
            }]
        },
        {
            xtype: 'tbseparator'
        },
        {
            text: 'Clean all filters',
            iconCls: 'x-fa fa-minus-circle',
            listeners: {
                click: 'onClickCleanFilters'
            }
        },

    ],

    dockedItems: [{
        xtype: 'toolbar',
        flex: 1,
        dock: 'top',
        items: [
            {
                xtype: 'textfield',
                emptyText: 'Enter name text...',
                fieldLabel: 'Name filter:',
                width: 350,
                id:'NameSearch',
                listeners: {
                    change : 'onChangeFilterName'
                }
            }
        ]
    }
    ],

    columns: [
        { text: '#',  dataIndex: 'number' },
        { text: 'id',  dataIndex: 'id' },
        { text: 'Name',  dataIndex: 'name', flex : 1 },
        { text: 'Start Date',  dataIndex: 'start-date', flex : 1 },
        { text: 'End Date',  dataIndex: 'end-date', flex : 1 },
        { text: 'Status',  dataIndex: 'status', flex : 1 },
        { text: 'Tasks count',  dataIndex: 'tasks-count', flex : 1 },
        { text: 'Description',  dataIndex: 'description', flex : 1 },
        { text: 'Created by',  dataIndex: 'created-by', flex : 1 }
    ],

    listeners: {
        select: 'onRowSelect'
    },

    initComponent: function(){
        this.selModel = Ext.create("Ext.selection.CheckboxModel", { checkOnly : false });
        this.callParent(arguments);
    }

});

