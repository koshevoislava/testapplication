Ext.define('MyApp.view.main.CampaingsContainer', {
    extend : 'Ext.container.Container',
    xtype: 'container',

    margin : 10,

    requires: [
        'MyApp.view.main.List',
        'MyApp.view.main.Form'
    ],
    layout : 'hbox',
    items: [
        {
           xtype : 'MainGrid',
            flex : 2
        },
        {
            xtype : 'DetailForm',
            flex : 1
        }
    ]

}

);

