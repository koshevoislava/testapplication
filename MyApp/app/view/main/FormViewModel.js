Ext.define('MVVM.view.FormViewModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.form',

    data : {
        rec : null
    }
});