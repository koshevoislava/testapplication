Ext.define('MyApp.view.main.Form', {
    extend : 'Ext.panel.Panel',


    xtype: 'DetailForm',
    id   : 'detailform',

    frame: true,
    bodyPadding : '25 20',
    margin : '0 0 0 20',

    viewModel : {
        type : 'form'
    },

    title:{
        text : 'Campaing form',
        bind : 'Campaing {rec.number}'
    },

        items : [
            {
                xtype       : 'displayfield',
                fieldLabel  : 'Id',
                disabled    : true,
                bind        : '{rec.id}',
                name        : 'id',
                value       : '0'
            },
            {
                xtype      : 'textfield',
                disabled   : true,
                bind       : '{rec.name}',
                fieldLabel : 'Name'
            },
            {
                xtype       : 'datefield',
                disabled    : true,
                anchor      : '100%',
                format      : 'm-d-Y H:i:s',
                bind        : '{rec.start-date}',
                fieldLabel  : 'Start date:',
                name        : 'start-date'
            },
            {
                xtype       : 'datefield',
                disabled    : true,
                format      : 'm-d-Y H:i:s',
                anchor      : '100%',
                bind        : '{rec.end-date}',
                fieldLabel  : 'End date:',
                name        : 'end-date'
            },
            {
                xtype       : 'textareafield',
                disabled    : true,
                grow        : true,
                width       : 500,
                bind        : '{rec.description}',
                name        : 'description',
                fieldLabel  : 'Description',
                anchor      : '100%'
            },
            {
                xtype:'fieldset',
                title: 'Mail notification',
                defaultType: 'textfield',
                items :[
                    {
                        xtype: 'checkboxgroup',
                        columns: 1,
                        id : 'main-checkboxes',
                        listeners: {
                            change: 'onChangeMailCheckbox'
                        },
                        vertical: true,
                        items: [
                            { boxLabel: 'Start & End of campaign', name: 'se-oc', disabled : true, bind: '{rec.se-oc}'},
                            { boxLabel: 'Start & End of tasks', name: 'se-ot', disabled : true,  bind: '{rec.se-ot}' },
                            { boxLabel: 'Failures', name: 'failures', disabled : true, bind: '{rec.failures}'}
                        ]
                    },
                    {
                        xtype     : 'textareafield',
                        grow      : true,
                        name      : 'сс',
                        fieldLabel: 'СС:',
                        bind      : '{rec.cc}',
                        id        : 'cc',
                        disabled : true,
                        vtype     : 'email',
                        anchor    : '100%'
                    }
                ]
            },
            {
                xtype  : 'button',
                margin : '0 10',
                disabled : true,
                text   : 'Submit',
                itemId : 'SubmitButton',
                listeners: {
                    click: 'onSavedClick'
                }
            },
            {
                xtype  : 'button',
                margin : '0 10',
                disabled : true,
                text   : 'Cancel',
                itemId : 'CancelButton',
                listeners: {
                    click: 'onCancelClick'
                }
            }
        ]

});

