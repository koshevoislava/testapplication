
Ext.define('MyApp.Application', {
    extend: 'Ext.app.Application',
    
    name: 'MyApp',

    stores: [
        'Campaings'
    ],

    view : [
        'MyApp.view.main.CampaingsContainer'
    ],
    
    launch: function () {
        Ext.create('Ext.tab.Panel', {

            controller : 'main',

            renderTo: document.body,

            items: [{
                title: 'Campaings',
                items: [
                    {
                        xtype: 'container'
                    }]
            }, {
                title: 'Tasks',
                html: 'Test'
            }, {
                title: 'Tasks History',
                html: 'Test'
            }]


        });

    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
